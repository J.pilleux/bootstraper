module Targets.Nvim (nvimTarget) where

import           Control.Monad.Trans.Resource (runResourceT)
import           Data.Conduit.Combinators     (sinkFile)
import           Network.HTTP.Conduit         (parseRequest)
import           Network.HTTP.Simple          (httpSink)

tmpFolder :: String
tmpFolder = "/tmp"

neoVimUrl :: String
neoVimUrl = "https://github.com/neovim/neovim/releases/nightly"

downloadNeoVim :: IO ()
downloadNeoVim = do
    request <- parseRequest

nvimTarget :: IO ()
nvimTarget = putStrLn "NVIM TARGET"

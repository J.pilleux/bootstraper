module Interpretter (targetInterpretter) where
import Targets.Nvim

notImplementedTarget :: String -> IO ()
notImplementedTarget target = putStrLn $ "NOT IMPLEMENTED : " ++ target

targetInterpretter :: String -> IO ()
targetInterpretter "nvim" = nvimTarget
targetInterpretter target = notImplementedTarget target

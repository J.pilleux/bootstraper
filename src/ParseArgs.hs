module ParseArgs
  ( getInputFileContent,
    isLineIgnorable,
  )
where

import Control.Monad
import Data.Char (isSpace)
import Data.List (isPrefixOf)
import Data.Maybe (fromMaybe)
import System.Console.GetOpt
import System.Environment (getArgs)
import System.Exit (exitSuccess)

data Flag
  = Help
  | Input String
  deriving (Show)

data Options = Options
  { optHelp :: Bool,
    optInputFile :: IO String,
    optOutput :: String -> IO ()
  }

startOptions :: Options
startOptions =
  Options
    { optHelp = False,
      optInputFile = getContents,
      optOutput = putStr
    }

options :: [OptDescr (Options -> IO Options)]
options =
  [ Option "h" ["help"] (NoArg (\opt -> return opt {optHelp = True})) "Help of the program",
    Option "o" ["output"] (ReqArg (\arg opt -> return opt {optOutput = writeFile arg}) "FILE") "Output file",
    Option "i" ["input"] (ReqArg (\arg opt -> return opt {optInputFile = readFile arg}) "FILE") "Input file"
  ]

printHelpAndExit :: IO ()
printHelpAndExit = do
  putStrLn "Help"
  exitSuccess

isLineIgnorable :: String -> Bool
isLineIgnorable s = "#" `isPrefixOf` s || null (dropWhile isSpace s)

getInputFileContent :: IO [String]
getInputFileContent = do
  args <- getArgs
  let (actions, nonOptions, errors) = getOpt RequireOrder options args

  opts <- foldl (>>=) (return startOptions) actions

  let Options
        { optHelp = help,
          optInputFile = input,
          optOutput = output
        } = opts

  when help printHelpAndExit

  fmap lines input

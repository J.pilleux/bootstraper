module Main where

import ParseArgs (getInputFileContent, isLineIgnorable)
import Interpretter (targetInterpretter)
import System.Exit (exitSuccess)

main :: IO ()
main = do
  content <- getInputFileContent
  let contentBis = filter (not . isLineIgnorable) content

  mapM_ targetInterpretter contentBis

  exitSuccess
